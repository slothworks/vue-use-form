# vue-use-form

Use form hook for handling forms and validations for Vue 3
with [composition API](https://github.com/vuejs/composition-api).

## Documentation

You can find a related documentation and
examples [here](https://gitlab.com/slothworks/vue-use-form/-/wikis/Documentation).

## Basic usage

```vue

<template>
  <form>
    <input
      v-model="fields['username']"
      v-bind="fieldHelpers['username'].$attrs"
      v-on="fieldHelpers['username'].$listeners"
      label="Username"
    />
    <input
      v-model="fields['password']"
      v-bind="fieldHelpers['password'].$attrs"
      v-on="fieldHelpers['password'].$listeners"
      label="Password"
      type="password"
    />
    <button
      label="Sign in"
      :loading="submitting"
      :disable="submitting"
      @click="signIn"
    />
  </form>
</template>

<script lang="ts">
import { defineComponent } from 'vue'
import { useForm } from '@slothworks/vue-use-form'
import { email, required, minLength } from '@vuelidate/validators'

interface Credentials {
  username: string
  password: string
}

export default defineComponent({
  name: 'SignInForm',
  setup () {
    const {
      fields,
      submitForm,
      submitting,
      fieldHelpers,
    } = useForm<Credentials>({
      model: {
        username: {
          defaultValue: '',
          validations: {
            required,
            email,
          },
        },
        password: {
          defaultValue: '',
          validations: {
            required,
            minLength: minLength(8),
          },
        },
      },
    })

    const signIn = () => submitForm(() => {
        // YOUR SIGN IN ACTION
      },
      (result) => {
        // ACTION ON SUCCESS
      },
      (error) => {
        // ACTION ON ERROR
      },
    )

    return {
      submitting,
      fields,
      fieldHelpers,
      signIn,
    }
  },
})
</script>
```

##API

Exported props:
```ts
interface IUseFormReturn<T> {
  // Values
  fields: T
  fieldHelpers: ComputedRef<FieldHelpersType<T>>
  initialValues: ComputedRef<T>
  // State
  isDirty: ComputedRef<boolean>
  isPristine: ComputedRef<boolean>
  submitting: ComputedRef<boolean>
  // Actions
  resetForm: () => void
  validateForm: () => Promise<boolean>
  submitForm: (
    callback: () => Promise<unknown>,
    onDone?: (result: unknown) => unknown,
    onError?: (error: unknown) => unknown,
  ) => Promise<unknown>
  reinitializeForm: (values: Partial<T>) => void
  setFieldValues: (values: Partial<T>) => void
  // Other
  vuelidate: Ref<Validation>
}
```

## Issues

All known issues are tracked [here](https://gitlab.com/slothworks/vue-use-form/-/issues)

## Links
[SlothWorks](https://slothworks.io/)
[Vue 3](https://v3.vuejs.org/)
[Vuelidate](https://vuelidate-next.netlify.app/)
