import { computed, ComputedRef, isProxy, reactive, ref, Ref, watch } from 'vue'
import {
  useVuelidate,
  Validation,
  ValidationRule,
  ValidationRuleWithoutParams,
  ValidationRuleWithParams,
} from '@vuelidate/core'
import { has, isEqual } from 'lodash'

type ValidationsType = Record<
  string,
  ValidationRule | ValidationRuleWithParams | ValidationRuleWithoutParams
>

type FieldsType<T> = {
  [P in keyof T]: Ref<T[P]>
}

interface IFieldHelper {
  $attrs: {
    name: string
    error: boolean
    errorMessage: string | Ref<string>
  }
  $listeners: {
    blur: () => void
  }
}

type FieldHelpersType<T> = Record<keyof T, IFieldHelper>

type FieldConfigType<D> = {
  defaultValue: D
  validations?: ValidationsType
}

export type UseFormModel<T> = {
  [P in keyof T]: Record<P, FieldConfigType<T[P]>>
}[keyof T]

interface IUseFormProps<T> {
  model: UseFormModel<T>
  initData?:
    | unknown
    | Partial<T>
    | Ref<T>
    | Readonly<T>
    | Readonly<Ref<T>>
    | Readonly<Ref<Readonly<T> | undefined>>
    | Ref<Partial<T>>
    | Readonly<Partial<T>>
    | Readonly<Ref<Partial<T>>>
    | Readonly<Ref<Readonly<Partial<T>> | undefined>>
}

interface IUseFormReturn<T> {
  // Values
  fields: T
  fieldHelpers: ComputedRef<FieldHelpersType<T>>
  initialValues: ComputedRef<T>
  // State
  isDirty: ComputedRef<boolean>
  isPristine: ComputedRef<boolean>
  submitting: ComputedRef<boolean>
  // Actions
  resetForm: () => void
  validateForm: () => Promise<boolean>
  submitForm: (
    callback: () => Promise<unknown>,
    onDone?: (result: unknown) => unknown,
    onError?: (error: unknown) => unknown,
  ) => Promise<unknown>
  reinitializeForm: (values: Partial<T>) => void
  setFieldValues: (values: Partial<T>) => void
  //  Other
  vuelidate: Ref<Validation>
}

const createFields = <T>({ model, initData }: IUseFormProps<T>): T => {
  const entries = Object.entries(model) as [
    [keyof T, FieldConfigType<T[keyof T]>],
  ]
  return reactive(
    entries.reduce((acc, [name, cfg]) => {
      const initValue = isProxy(initData)
        ? (initData as Ref<T>)?.value?.[name]
        : (initData as Partial<T>)?.[name] || cfg.defaultValue

      acc[name] = ref(initValue) as Ref<T[keyof T]>

      return acc
    }, {} as FieldsType<T>),
  ) as T
}

const createFieldHelpers = <T>(
  model: UseFormModel<T>,
  vuelidate: Ref<Validation>,
): ComputedRef<FieldHelpersType<T>> => {
  const keys = Object.keys(model) as (keyof T)[]
  return computed(() =>
    keys.reduce((acc, key) => {
      const touch = () => vuelidate.value?.[key as string]?.$touch?.()
      const error = !!vuelidate.value?.[key as string]?.$errors?.length
      const errorMessage =
        vuelidate.value?.[key as string]?.$errors?.[0]?.$message || ''

      acc[key] = {
        $attrs: {
          name: key as string,
          error,
          errorMessage: errorMessage,
        },
        $listeners: {
          blur: () => {
            touch()
          },
        },
      }
      return acc
    }, {} as FieldHelpersType<T>),
  )
}

const createInitialValues = <T>({ model, initData }: IUseFormProps<T>): T => {
  const entries = Object.entries(model) as [
    [keyof T, FieldConfigType<T[keyof T]>],
  ]
  return reactive(
    entries.reduce((acc, [name, cfg]) => {
      const initValue = isProxy(initData)
        ? (initData as Ref<T>)?.value?.[name]
        : (initData as Partial<T>)?.[name] || cfg.defaultValue

      acc[name] = initValue as T[keyof T]

      return acc
    }, {} as Record<keyof T, T[keyof T]>),
  ) as T
}

const createComputedInitialValues = <T>(initialValues: T): ComputedRef<T> => {
  return computed(() => initialValues)
}

const getRules = <T>(
  model: UseFormModel<T>,
): Record<string, ValidationsType> => {
  const entries = Object.entries(model) as [
    [string, FieldConfigType<T[keyof T]>],
  ]
  return entries.reduce((acc, [name, cfg]) => {
    if (cfg.validations) {
      acc[name] = cfg.validations
    }
    return acc
  }, {} as Record<string, ValidationsType>)
}

export function useForm<T>({
  model,
  initData,
}: IUseFormProps<T>): IUseFormReturn<T> {
  const fields = createFields<T>({
    model,
    initData,
  })
  const rules = getRules<T>(model)
  const vuelidate = useVuelidate<Record<string, ValidationsType>, T>(
    rules,
    fields,
  )
  const fieldHelpers = createFieldHelpers<T>(model, vuelidate)
  const _initialValues = createInitialValues<T>({
    model,
    initData,
  })
  const initialValues = createComputedInitialValues<T>(_initialValues)

  const _submitting = ref(false)

  const submitting = computed(() => _submitting.value)

  const isDirty = computed(() => {
    return !isEqual(fields, _initialValues)
  })

  const isPristine = computed(() => {
    return isEqual(fields, _initialValues)
  })

  // ACTIONS
  const resetForm = (): void => {
    const keys = Object.keys(fields) as (keyof T)[]
    keys.forEach((key) => {
      fields[key] = _initialValues[key]
    })
    vuelidate.value.$reset()
  }

  const setFieldValues = (values: Partial<T>) => {
    const entries = Object.entries(values) as [keyof T, T[keyof T]][]
    entries.forEach(([key, value]) => {
      if (has(_initialValues, key)) {
        fields[key] = value
      }
    })
  }

  const setInitialValues = (values: Partial<T>) => {
    const entries = Object.entries(values) as [keyof T, T[keyof T]][]
    entries.forEach(([key, value]) => {
      if (has(_initialValues, key)) {
        _initialValues[key] = value
      }
    })
  }

  const reinitializeForm = (values: Partial<T>) => {
    setFieldValues(values)
    setInitialValues(values)
    vuelidate.value.$reset()
  }

  const validateForm = async (): Promise<boolean> => {
    return await vuelidate.value.$validate()
  }

  const submitForm = async (
    callback: () => Promise<unknown>,
    onDone?: (result: unknown) => unknown,
    onError?: (error: Error) => unknown,
  ) => {
    if (await validateForm()) {
      try {
        _submitting.value = true
        const result = await callback()
        onDone?.(result)
        return result
      } catch (e) {
        onError?.(e)
      } finally {
        _submitting.value = false
      }
    }
  }

  //INIT DATA WATCHER
  watch([initData], () => {
    if (initData) {
      const _initData = initData as Ref<T>
      setFieldValues(_initData.value)
      setInitialValues(_initData.value)
    }
  })

  return {
    fields,
    fieldHelpers,
    initialValues,
    // Helper state
    isDirty,
    isPristine,
    submitting,
    // Actions
    resetForm,
    validateForm,
    submitForm,
    reinitializeForm,
    setFieldValues,
    // Other
    vuelidate,
  }
}
